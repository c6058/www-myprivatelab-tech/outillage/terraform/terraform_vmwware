#Variables globales

variable "vsphere_user" {
  default = "user@vsphere"
}

variable "vsphere_password" {
  default = "password_vsphere_user"
}

variable "vsphere_server" {
  default = "server.vcenter.priv"
}

variable "vsphere_datacenter" {
  default = "nom_datacenter_vsphere"
}

variable "vsphere_cluster" {
  default = "nom_vsphere_clustter"
}
