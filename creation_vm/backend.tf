terraform {
  cloud {
    organization = "TERRAFOM_CLOUD_ORG"

    workspaces {
      name = "WORKSPACE_NAME"
    }
  }
}